package school;

//importing a package
import calculator.Calculate;

class ExpensesDetail extends SchoolManagment{
	private double totalExpenses;
	private double totalSalaries;
	
	public ExpensesDetail(String name, String address, int fixedSalaries, double incentives, double buldingRent, double electricityBills) {
		super(name, address);
		setSalary(fixedSalaries,incentives);
		totalExpenses = computeExpenses(totalSalaries, buldingRent, electricityBills);
		System.out.println("Name: " + name + "\nAddress: " +  address + "\nSalary expenses: " + totalSalaries + "\nTotal expenses: " + totalExpenses);
	}
	
	
	public double getSalary() {
	      return totalSalaries;
	   }
	
	//Set fixed salary
	public void setSalary(int fixedSalaries) {
	      if(fixedSalaries >= 0.0) {
	    	  totalSalaries = fixedSalaries;
	      }
	      
	   }
	
	//overloading 
	//total salary fixed + incentive
	public void setSalary(int fixedSalary, double incentive) {
		   
		totalSalaries = fixedSalary + incentive;
		   
		    /* This is not visible at this has default access modifier 
		     * Default access modifier visible in same package and here calculator is different package*/
		       
		      /* Calculate cal = new Calculate();
	    	   totalSalaries = cal.add(fixedSalary, incentive); */
	    	
	      
	   }
	
	//overriding 
	//total expenses of school
	public double computeExpenses( double salary, double buildingRent, double electricityBills) {
		Calculate cal = new Calculate(); //Here add function is public 
		totalExpenses = cal.add(getSalary(), buildingRent, electricityBills);
		return totalExpenses;
	}

}
