package school;
 enum Day{
	 SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
 }
public abstract class SchoolManagment {
	private String name;
	private String address;
	Day day;

	public abstract double computeExpenses(double totalSalaries, double buildingRent, double elcticityBills);

	
	public SchoolManagment(String name, String address) {
		System.out.println("School details are as below:");
		this.name = name;
		this.address = address;
	}
	
	// Print school dress code color day wise using switch
	
	public void dayWiseDressCode(Day day) {
		switch(day) {
		case MONDAY:
			System.out.println("Monday's dress code is white");
			break;
		case TUESDAY:
			System.out.println("Tuesday's dress code is red");
			break;
		case WEDNESDAY:
			System.out.println("Wednesday's dress code is green");
			break;
		case THURSDAY:
		case FRIDAY:
			System.out.println("Thursday & Friday's dress code is yellow");
			break;
		case SATURDAY:
			System.out.println("Thursday & Friday's dress code is black/blue");
			break;
		default:
			System.out.println("Holiday");
			break;
			
		}
	}
	
}

