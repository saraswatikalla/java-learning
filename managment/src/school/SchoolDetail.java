package school;

import calculator.Calculate;

public class SchoolDetail extends Calculate {

	public static void main(String[] args) {
		ExpensesDetail e =  new ExpensesDetail("Dolphin", "Sodala", 945444, 457834.89,983894.89,3485734.98);
		String str = "MONDAY";
		e.dayWiseDressCode(Day.valueOf(str));
		
		//Can't call directly protected method in different package directly
		/*Calculate cal =  new Calculate();
		cal.calculateArea(987.98,897.9); */
		
		SchoolDetail c =  new SchoolDetail();
		c.calculateArea(789.89, 897.78);
	}

}
