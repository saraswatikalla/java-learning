package collections_examples;
import java.util.*;

public class ArrayListExample {

	public static void main(String[] args) 
	{
		ArrayList a1 = new ArrayList(); // creating array list
		
		/* Syntax to create all collections */
		
		/* LinkedList<String> al=new LinkedList<String>();  // creating linked list
		 * 
		 * Vector object = new Vector(size,increment);scvbnm 
		 * 
		 */
		
		a1.add("Jack");
		a1.add("Tyler");
		Iterator itr = a1.iterator();
		
		while(itr.hasNext()) {
			System.out.print(itr.next() + "\n");
		}

	}

}
