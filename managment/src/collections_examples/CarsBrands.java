package collections_examples;

import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.HashMap;
import java.util.Map;

public class CarsBrands {
	static Scanner scan = new Scanner(System.in);
	static HashMap<Integer, String> carsList = new HashMap<>();
	public static void main(String[] args) {
		int option;
		System.out.println("Enter option number \n1. To see list of cars \n2. Add cars in list \n3. Edit cars list \n4.Remove car from list");
		try {
			option = scan.nextInt();
			operation(option);
		}catch(InputMismatchException e) {
			System.out.print("Wrong input. Try again with option number after rerunning application.");
		}
	}
	private static void operation(int option) {
		switch(option)
		{
			case 1:
				System.out.println("List of cars: " + carsList);
				int choice = userInputInt();
				isContinue(choice);
				break;
			case 2:			
				System.out.println("Enter no. of cars you want to add in the list: ");
				int numOfCars = scan.nextInt();
				System.out.println("Enter " + numOfCars + " cars name: ");
				for(int i = 0; i<numOfCars; i++) {				
					String carName = scan.next();
					carsList.put(i, carName);
				}
				System.out.println("Cars added successfuly.\nlist is as below :\n" + carsList);
				int choice2 = userInputInt();
				isContinue(choice2);
				break;
			case 3:
				System.out.println("Enter car option number you want to edit");
				int editCarNo = scan.nextInt();
				// Check if a key exists in a HashMap	
				if(carsList.containsKey(editCarNo)) {
					System.out.println("Enter car name: ");
					String carNewName = scan.next();
					if(carNewName !="") {
						carsList.put(editCarNo, carNewName);
						System.out.println("\nModified list of cars: " + carsList);
					}else {
						System.out.println("There is problem to edit list.");
					}
				}else {
					System.out.println("This value not exist in the list");
				}
				int choice3 = userInputInt();
				isContinue(choice3);
				break;
			case 4:
				System.out.println("Enter car number you want to remove: ");
				int carNo = scan.nextInt();
				// Check if a value exists in a HashMap	
				if(carsList.containsKey(carNo)) {
					carsList.remove(carNo);
					System.out.println("Car name removed from list successfully.");
				}else {
					System.out.println("This value not exist in the list");
				}
				System.out.println("\nModified list of cars: " + carsList);
				int choice4 = userInputInt();
				isContinue(choice4);
				break;
			default:System.out.println("Option no. you chosen is out of scope.");
			int choice5 = userInputInt();
			isContinue(choice5);
			break;
		}
	}
	private static void isContinue(int choice) {
		if(choice ==0) {
			System.out.println("Thank you for visting!!");
		}else if (choice ==1 || choice ==2 || choice ==3 || choice ==4 ){
			operation(choice);
		}else {
			System.out.println("Option no. you chosen is out of scope.");
			operation(choice);
		}
	}
	private static int userInputInt() {
		int isContinue;
		try {
			System.out.println("Do you want to continue, choose option no. as above or 0 for exit: ");
			 isContinue = scan.nextInt();
		}catch(InputMismatchException e) {	
			System.out.print("Wrong input. Try again with option number after rerunning application.");
			isContinue = userInputInt();
		}
		return isContinue;
	}
}
