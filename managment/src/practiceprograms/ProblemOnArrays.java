package practiceprograms;

import java.util.ArrayList;

public class ProblemOnArrays {
	
	
/*
 * Eg:1 Find group of max sum with max length [5, 6, 9, 8, -12, 4, 3, 9, 8, -1, 9, 8,9,8,3]
*/
	int maxSum = 0;
	
	int seqSum = 0;

	
	public void seqSum(int [] seq) {
		
		ArrayList<Integer> seqOfSum = new ArrayList<>();
		ArrayList<Integer> maxSeq = new ArrayList<>();
		
		System.out.println("Array length is: " + seq.length);
		
		for(int i=0; i<seq.length; i++) {
			
			if(seq[i] >= 0) {
				seqSum += seq[i];
				seqOfSum.add(seq[i]);
				 
				if(seqSum > maxSum) {
					 maxSum = seqSum;
					 maxSeq = seqOfSum;
				} else if (seqSum == maxSum && seqOfSum.size() > maxSeq.size()) {
					 maxSeq = seqOfSum;
					 maxSum = maxSum;
				}else {
					maxSeq = maxSeq;
					maxSum = maxSum;
				}
			 }else {
				 seqSum = 0;
				 seqOfSum = new ArrayList<>();
				 continue;
			 }
		}
		
		System.out.println("Max sum of seq is: " + maxSum );
		System.out.println("Seq of max sum is: " + maxSeq);
		
	}
	
/* --end --*/	

}
