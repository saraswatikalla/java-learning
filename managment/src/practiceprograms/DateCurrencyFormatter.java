package practiceprograms;
import java.util.Scanner;
import java.util.Locale;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Currency;
import java.lang.Math;



public class DateCurrencyFormatter {

	public static void main(String[] args) {
		Scanner scanner =  new Scanner(System.in);
		
		/* Date Format */
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.UK);
		
		System.out.println("Enter date and time in the format yyyy-MM-ddTHH:mm");
		System.out.println("For example, it is now " + format.format(new Date()));
		
		Date date = null;
		while(date == null) {
			String line = scanner.nextLine();	
			try {
				date = format.parse(line);
				System.out.println(date);
			}catch(ParseException e) {
				System.out.println("Sorry, that's not valid. Please try again.");
			}
		}
		/* --END-- */
		
		/* Currency Locale */
		
		System.out.println("\nEnter Number to convert Rupee to Dollar");
		
		Scanner scan = new Scanner(System.in);
		
		Double number = scan.nextDouble();
		
		//Create a currency for US locale
		Locale localeUS = Locale.US;
		
		Currency currUS = Currency.getInstance(localeUS);
		
		//get and print the symbol of the currency
		
		String symbolUS = currUS.getSymbol(localeUS);
		
		
		System.out.println(number + "Rs is equal to " + Math.round(number/71.94) +symbolUS );
		
		/* --END-- */
		
		
	}
	

	
}
