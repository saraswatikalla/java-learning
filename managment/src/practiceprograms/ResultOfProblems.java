package practiceprograms;
import java.util.ArrayList;
public class ResultOfProblems {
	
	
	public static void main(String[] args) {
		ProblemOnArrays a = new ProblemOnArrays();
		
		/*
		 * Eg:1 Find group of max sum with max length [5, 6, 9, 8, -12, 4, 3, 9, 8, -1, 9, 8,9,8,3]
		 */
		int []seq = {1, 1, 19, -12, 4, 3, 6, 8, -1, 9,8};
		
		a.seqSum(seq);
		
		/* -end- */
		
		/* eg:
		  *  Find sequence of char in alphabetical order from string {hjktuvkjlduvlkmn}
		  */
			
		 ProgramOfString ps = new ProgramOfString();
		
		 ps.alphaOrder("defksdfsdtuvlkskdfjklsd");
		
		/* --END-- */
		 
		 
	/* Eg:3 Patterns */
		 
		PatternPrograms pp =  new PatternPrograms();
		
		pp.PrintTriangle(5);
		
		pp.Pyramid(6);
		
	}
	

}
