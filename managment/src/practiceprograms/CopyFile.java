package practiceprograms;

import java.io.*;
import java.io.IOException;
 
/**
 * This program demonstrates how to write characters to a text file.
 * @author www.codejava.net
 *
 */
public class CopyFile {
 
    public static void main(String[] args) throws Exception{
        try {
            FileWriter writer = new FileWriter("FinalTest.txt", true);
            writer.write("Hello World");
            writer.write("\r\n");   // write new line
            writer.write("Good Bye!");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        File file = new File("FinalTest.txt"); 
        
        BufferedReader br = new BufferedReader(new FileReader(file)); 
        
        String st; 
        while ((st = br.readLine()) != null) 
          System.out.println(st); 
 
    }

}