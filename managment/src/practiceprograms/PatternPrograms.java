package practiceprograms;

public class PatternPrograms {
	
	public void PrintTriangle(int count) {
		for(int i=1; i<=count; i++) {
			for(int j=1; j<i; j++) {
				System.out.print("*");
			}
			
			System.out.println();
		}
	}
	
	public void Pyramid(int rows) {
		int i, space, k=0;
	    for(i =1; i<=rows; i++) {
	    	for (space = 1; space<=(rows-i); space++) {
				
	    		System.out.print("  ");
			}
	    	
	    	while(k != (2*i-1)) {
	    		 System.out.print("+ ");
	    		 k++;
	    	}
	    	
	    	k =0;
	    	System.out.println();
	    }
		
	}
	
	

}
