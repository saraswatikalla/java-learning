package practiceprograms;

import java.util.ArrayList;
import java.util.Arrays;

public class ProgramOfString {
 
 /* eg:
  *  Find sequence of char in alphabetical order from string {hjktuvkjlduvlkmn}
  */
	
	public static void main(String[] args) {
		String str =  "fndkabnopabstvlnstv";
		int i=0;
		String result = getAlphabeticalSeq(str, i);
		
			System.out.println(result);
	
		//System.out.println("Alphabeticle order substrings : " + getAlphabeticalSeq(str, i));
	}
	
	
	private static String getAlphabeticalSeq(String str, int i) {
		
		String result = "";
		String subStr = "";
		
		if (i == str.length()) {
			return "";
		}else {
			subStr += str.charAt(i);
			
			result +=getAlphabeticalSeq(str, i+1);
			
			return result;
		}
	}
	
	
	public void alphaOrder(String alphaString) {
		String subString = "";
		char[] newSubStr = subString.toCharArray();
		ArrayList<String> setOfSubStr = new ArrayList<>();
		char previous = '\u0000';
		for(int i =0; i<alphaString.length(); i++) {
			 subString +=  alphaString.charAt(i);
			if(subString.length() >= 2) {
				boolean sorted = isInOrder(previous, newSubStr);
				if(sorted == true) {
					setOfSubStr.add(subString);
				}else {
					subString ="";
					continue;
				}
			}
		}
		
		System.out.println("Sub String " + setOfSubStr);
	}
	
	private static boolean isInOrder(char previous, char[]arr) {
		for(char current: arr) {
			if(current<previous)
				return false;
			previous = current;
		}
		return true;
	}
	
	
}
