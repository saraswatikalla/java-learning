package bank;

// Program to demonstrate user defined exception

// This program throws an exception whenever balance amount is below 1000

public class CustomExcpetion extends Exception {
	
	//account information
	
	private static int accno[] = {1001, 1002, 1003, 1004};
	
	private static String name[] =
		{"Nisha", "Shubha", "Sush", "Abhi", "Akash"};
	
	private static double bal[]=
		{1000.00, 12000.00, 5600.0, 999.00, 1100.55};
	
	//Default constructor
	
	CustomExcpetion(){
		
	}
	
	//Parametrized constructor
	
	CustomExcpetion(String str){
		super(str);
	}
	
	public static void main(String[] args) {
		try {
			//Display the heading for the table
			System.out.println("ACCNO" + "\t" + "CUSTOMER" + "\t" + "BALANCE");
			
			//Display the actual account information
			
			for(int i =0; i<5; i++) {
				 System.out.println(accno[i] + "\t" + name[i] + "\t" + bal[i]);
				 
				 //display own exception if balance < 1000;
				 
				 if(bal[i] < 1000) {
					 CustomExcpetion me = new CustomExcpetion("Balance is less than 1000");
					 throw me;
				 }
			}
		}
		catch(CustomExcpetion e){
			e.printStackTrace();
		}

	}

}
