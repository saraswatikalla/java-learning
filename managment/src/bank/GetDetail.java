package bank;

import java.util.Arrays;

public class GetDetail {

	public static void main(String[] args) {
		short age =25;
		byte numOfAccount = 4;
		Customer c =  new Customer("Ram", age, numOfAccount, 109876677777l, 7.3f);
	  /*Array Searching & Sorting*/
		double customerBalance[] = {92345,89767,897893,890362};
		Arrays.sort(customerBalance);
		printBalance("Balance of customer in increasing order", customerBalance);
		int index = Arrays.binarySearch(customerBalance, 897893);
		System.out.println("Found 897893 @" + index);
	  /* -end- */
	  /* Search a word in string object */
		String customerDetail = "Ram's account details";
		int position = customerDetail.indexOf("Ram");
		if(position == -1) {
			System.out.println("Ram not found");
		}else {
			System.out.println("Found Ram at index " + position);
		}
	  /* -end- */
	 /* Find is objects equals or not */
		Employee e1 = new Employee("Mayank", 29, "6789987");
		Employee e2 = new Employee("Mayank", 29, "6789987");
		System.out.println("\nIs " + e1 + ", "+ e2 + " equal: " + e1.equals(e2));
		}
	/* Array  Sorting & Searching result print */	
	private static void printBalance(String message, double customerBalance[]) {
		System.out.println(message + ":[length:" + customerBalance.length + "]");
		for(int i =0; i<customerBalance.length; i++) {
			if(i !=0) {
				System.out.println(", ");
			}
			System.out.println(customerBalance[i]);
		}
		System.out.println();
	}
	/* -end- */	
}
