package bank;

public class Customer {
	private String name;
	private short age;
	private byte numOfAccount;
	private long accountNumber;
	private float interestRate;
	
	
	
	public Customer(String name, short age, byte numOfAccount, long accountNumber, float interestRate) {
		setCustomerDetail(name, age, numOfAccount, accountNumber, interestRate);
	}
	
	
	
	public void setCustomerDetail(String name, short age, byte numOfAccount, long accountNumber, float interestRate) {;
		System.out.println("Name: " + name + "\nAge: " + age + "\nNo. of account in this bank: " + numOfAccount + "\nCurrent account number: " + accountNumber + "\nCurrent account interest rate: " + interestRate);
		
	}
	
	
	
}
