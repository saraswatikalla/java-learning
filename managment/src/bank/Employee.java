package bank;

import java.util.Objects;

public class Employee {
	private String name;
	private int age;
	private String passport;
	
	public Employee(String name, int age, String passport) {
		this.name = name;
		this.age = age;
		this.passport = passport;
	}
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if(!(o instanceof Employee)){
			return false;
		}
		Employee emp = (Employee) o;
		return age == emp.age && Objects.equals(this.name, emp.name) && Objects.equals(this.passport, emp.passport);
	}
	@Override
	public int hashCode() {
		return Objects.hash(this.name, this.age, this.passport);
	}
}
