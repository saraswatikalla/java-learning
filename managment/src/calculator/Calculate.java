package calculator;

public class Calculate {
	private double width, height;
	private double area;
	private double currentLandRate;
	private  double addition;
	private int landRate;
	private double elctricityBill;
	private double powerRate;
	
	// constructor visible to public
	public Calculate() {
		landRate = 1000;
		powerRate = 6.7;
	}
	
	// Constructor overloading
	 Calculate(double consumption ){
		 this();
		 elctricityBill = powerRate * consumption;
		 System.out.println("Electricity consumption expenses: " + elctricityBill);
	 }
	
	
	private double calculateRate(double area) {
		currentLandRate = landRate*area;
		return currentLandRate;
	}
	
	//Area of rectangle 
	protected double calculateArea(double width, double height) {
			area =  width * height;
			System.out.println("Area of rectuanglar shape school which have width, length recspectively are " + width +", "+ height +" is: " + area);
			return area;
		}
		

	public double add (double value1, double value2, double value3) {
		return value1 + value2 + value3;
	}
	
	//only visible in same package;
	double add(int value1, double value2) {
		addition = value1 + value2;
		System.out.println("Adition of two values "+ value1 + ", " + value2 + " are: " + addition);
		return addition;
	}
	
	public double getLandExpense(double area) {
		System.out.println("Total expense of land area "+ area + " sq ft in Rs is " + calculateRate(area));
		return area;
	}

}
