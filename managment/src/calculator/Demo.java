package calculator;

public class Demo{

	public static void main(String[] args) {
		
		Calculate cal = new Calculate();
		
		//Add to numbers
		cal.add(345, 456);
		
		//Calculate rectangular shape
		cal.calculateArea(87.98, 789.8); // protected method can be called directly in same package
		
		//cal.calculateRate(897.90) // private method can't called directly
		
		//Calculate land expenses by providing area
		cal.getLandExpense(678.9);
		
		//get electricity bill by providing consumption units
		Calculate exp =  new Calculate(789.9);
		
	}

}
